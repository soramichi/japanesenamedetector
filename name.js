#!/usr/local/bin/node

const EOS = 0;
const VOWEL = 1;
const CONSONANT = 2;
const ILLEGAL = 3;

var vowels = ["a", "i", "u", "e", "o", "oh"];
var consonants = ["b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "q", "r", "s", "t", "v", "w", "x", "y", "z", "ch", "ts", "sh"];

function find_longest_prefix(str, array){
    var ret = "";
    var max_length = 0;

    for(var i=0;i < array.length;i++){
	if(str.substr(0, array[i].length) == array[i] && max_length < array[i].length){
	    ret = array[i];
	    max_length = array[i].length;
	}
    }

    return ret;
}

function get_next_token(str, start){
    var index;
    var token;

    if(start == str.length){
	return [EOS, "", 0];
    }
    else if((token = find_longest_prefix(str.substr(start), vowels)) != ""){
	return [VOWEL, token, start + token.length];
    }
    else if((token = find_longest_prefix(str.substr(start), consonants)) != ""){
	return [CONSONANT, token, start + token.length];
    }
    else{
	return [ILLEGAL, "", 0];
    }
}

function test(str){
    var index = 0;
    var score = 1.0;
    var type_previous = EOS;
    var token_previous = "";

    str = str.toLowerCase();

    while(true){
	var ret = get_next_token(str, index);
	var type = ret[0];
	var token = ret[1];
	index = ret[2];

	/**************** Scoring *********************/
	// later use trasition matrix learned from real data

	// a sequence of two consonants is illegal, except when the former one is "n"
	// note that legal sequence of consonants (e.g. "ch") is regarded as a CONSONANT
	if(type_previous == CONSONANT && type == CONSONANT && token_previous != "n")
	    score = 0.0;

	// the end of a name must not be a consonant (except "n")
	if(type == EOS && type_previous == CONSONANT && token_previous != "n")
	    score = 0.0
	/**********************************************/

	type_previous = type;
	token_previous = token;

	if(type == EOS)
	    break;
    }

    return score;
}


console.log(test("Soramichi")); // a JP name
console.log(test("David"));     // a non-JP name, the rules work
console.log(test("Jonathan"));  // another non-JP name, the rules work
console.log(test("Chen"));      // maybe a non-JP name, but the rules don't work. needs learning transit matrix from real names
