#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h>

const int START_OF_WORD = ('z' - 'a') + 1;
const int END_OF_WORD = ('z' - 'a') + 2;
const int n_elements = ('z' - 'a' + 1) + 2; // +2 is for START_OF_WORD and END_OF_WORD

int count[n_elements][n_elements];
double transition[n_elements][n_elements];

void learn(){
  char name[32];
  int max = 0;
  FILE* input = fopen("Name/Japanese/names_learn.dat", "r");
  FILE* output = fopen("transition.dat", "w");

  if(input == NULL || output == NULL){
    perror("fopen");
    exit(1);
  }

  for(int i=0;i<n_elements;i++){
    for(int j=0;j<n_elements;j++){
      count[i][j] = 0;
      transition[i][j] = 0.0;
    }
  }

  // learning
  while(fgets(name, sizeof(name), input)){
    int character, character_previous = START_OF_WORD;

    for(int i=0;i<strlen(name) - 1;i++){ // name includes a '\n' at the end of it, thus -1 for the loop condition
      character = tolower(name[i]) - 'a';
      count[character_previous][character]++;
      character_previous = character;
    }

    count[character][END_OF_WORD]++;
  }

  // normalize
  for(int i=0;i<n_elements;i++){
    for(int j=0;j<n_elements;j++){
      transition[i][j] = count[i][j];

      if(count[i][j] > max)
	max = count[i][j];
    }
  }
  for(int i=0;i<n_elements;i++){
    for(int j=0;j<n_elements;j++){
      transition[i][j] /= max;
    }
  }


  // output the normalize transition matrix
  for(int i=0;i<n_elements;i++){
    for(int j=0;j<n_elements;j++){
      fprintf(output, "%.4f", transition[i][j]);
      if(j != n_elements - 1)
	fprintf(output, ",");
    }
    fprintf(output, "\n");
  }

  fclose(input);
  fclose(output);
}

void print(){
  // print statistics
  printf(" ");

  for(int i=0;i<n_elements;i++){
    if(i <= 'z' - 'a')
      printf("   %c", 'a' + i);
    else
      printf("   #");
  }

  puts("");

  for(int i=0;i<n_elements;i++){
    if(i <= 'z' - 'a')
      printf("%c", 'a' + i);
    else
      printf("#");

    for(int j=0;j<n_elements;j++){
      printf("%4d", count[i][j]);
    }
    puts("");
  }
}

int main(int argc, char* argv[]){
  char buff[32];

  learn();

  while(fgets(buff, sizeof(buff), stdin)){
    double ans = 1.0;
    int character, character_previous = START_OF_WORD;

    for(int i=0;i<strlen(buff);i++){
      // delete the '\n' at the end of a line, if any
      if(buff[strlen(buff) - 1] == '\n')
	buff[strlen(buff) - 1] = '\0';

      character = tolower(buff[i]) - 'a';
      ans *= transition[character_previous][character];
      character_previous = character;
    }
    ans *= transition[character][END_OF_WORD];
    
    // normalize, otherwise longer and longer the given name is lower and lower the score becomes
    ans = pow(ans, 1.0 / strlen(buff));

    printf("%s %.10f\n", buff, ans);
  }
}
